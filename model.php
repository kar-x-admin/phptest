<?php namespace model;
class PgDB {
    public $db;

    function __construct (){
        $host        = "host = 127.0.0.1";
        $port        = "port = 5432";
        $dbname      = "dbname = phpdb";
        $credentials = "user = phpuser  password=php123";

        $this->db = pg_connect( "$host $port $dbname $credentials" );


        $createTables=array("CREATE TABLE IF NOT EXISTS authors 
        (id SERIAL PRIMARY KEY, author VARCHAR(100) NOT NULL, UNIQUE(author));",
        "CREATE TABLE IF NOT EXISTS books 
        (authorid integer,  book VARCHAR(100) NOT NULL);");
        foreach ($createTables as $sql){
            pg_query($this->db, $sql);
        }
    }

    function __destruct() {
        pg_close($this->db);
    }
}
