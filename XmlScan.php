<?php
include "model.php";

class XMLtoDB extends model\PgDB{
    private function SendSql($sql){
        $ret = pg_query($this->db, $sql);
        return pg_fetch_result($ret,0);
    }
    function AddBook(string $author,string $book){
        if(!(empty($author) or empty($book))){
            $readsql="SELECT id FROM authors WHERE author = '$author'";
            $id= $this -> SendSql($readsql);
            if(!$id){
                $sql="INSERT INTO authors (author) VALUES ('$author')
                ON CONFLICT (author) DO NOTHING
                RETURNING id";
                $id = $this -> SendSql($sql);
            }
            $sql="SELECT authorid FROM books WHERE authorid = '$id' AND book ='$book'";
            if (! $this -> SendSql($sql)){
                $sql="INSERT INTO books VALUES ('$id','$book')";
                $this -> SendSql($sql);
            }
        }
    }
}

$Pg = new XMLtoDB();
# SET Main Dir here
$path='~/';
exec("find $path -name *.xml",$xmllist);
foreach($xmllist as $file){
    if(is_file($file) and $xml=simplexml_load_file($file)){
        foreach($xml->book as $book){
            $Pg->AddBook($book->author, $book->name);
        }
    }
}